<?php

class Building {

    //1. Create the necessary getter and setter functions for the floors and address properties of Condominium.

    //2. Change your code so that a building's floor and address property cannot be changed after creation.

    //3. Finally, change your code in such a way that will let both the classes use the getter and setter functions that you have created.

    public $name;
    public $floor;
    public $address;

    public function __construct($name, $floor, $address ) {

        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    public function getName() {

        return $this->name;
    }

    public function getFloor() {

        return $this->floor;
    }

    public function getAddress() {

        return $this->address;
    }




    public function setName($name){

        if(gettype($name) === "string"){
             $this->name = $name;
           
        }
       
    }
    
    public function setFloor($floor){

        if(gettype($floor) !== "string"){
             $this->floor = $floor;
           
        }
       
    }

    public function setAddress($address){

        if(gettype($address) === "string"){
             $this->address = $address;
           
        }
       
    }


}

class Condominium extends Building {

  
    public function getName() {

        return $this->name;
    }

    public function getFloor() {

        return $this->floor;
    }

    public function getAddress() {

        return $this->address;
    }




    public function setName($name){

        if(gettype($name) === "string"){
             $this->name = $name;
           
        }
       
    }
    
    public function setFloor($floor){

        if(gettype($floor) == number){
             $this->floor = $floor;
           
        }
       
    }

    public function setAddress($address){

        if(gettype($address) === "string"){
             $this->address = $address;
           
        }
       
    }
}

$building = new Building('Caswynn Building',8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo',5, 'Buendia Avenue, Makati City, Philippines');